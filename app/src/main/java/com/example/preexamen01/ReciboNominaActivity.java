package com.example.preexamen01;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class ReciboNominaActivity extends AppCompatActivity {
    // Declaración de variables
    private EditText txtNumRecibo;
    private EditText txtNombre;
    private EditText txtHorasNormal;
    private EditText txtHorasExtra;

    private RadioButton rbAuxiliar;
    private RadioButton rbAlbañil;
    private RadioButton rbIngObra;

    private TextView lblUsuario;
    private EditText txtSubtotal;
    private EditText txtImpuesto;
    private EditText txtTotalPagar;

    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    private ReciboNomina reciboN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);
        iniciarComponentes();
        // Obtener los datos del MainActiviy
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        lblUsuario.setText(nombre);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
    }

    private void iniciarComponentes() {
        // Relacionar los objetos
        txtNumRecibo = findViewById(R.id.txtNumRecibo);
        txtNombre = findViewById(R.id.txtNombre);
        txtHorasNormal = findViewById(R.id.txtHorasNormal);
        txtHorasExtra = findViewById(R.id.txtHorasExtra);
        txtSubtotal = findViewById(R.id.txtSubtotal);
        txtImpuesto = findViewById(R.id.txtImpuesto);
        txtTotalPagar = findViewById(R.id.txtTotalPagar);

        lblUsuario = findViewById(R.id.lblUsuario);

        rbAuxiliar = findViewById(R.id.rbAuxiliar);
        rbAlbañil = findViewById(R.id.rbAlbañil);
        rbIngObra = findViewById(R.id.rbIngObra);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        reciboN = new ReciboNomina(0, "", 0, 0, 0, 0);

        deshabilitarTextos();
    }

    private void calcular() {
        int puesto = 0;
        float horasNormales = 0;
        float horasExtra = 0;
        float subtotal = 0;
        float impuesto = 0;
        float total = 0;

        if(rbAuxiliar.isChecked()) {
            puesto = 1;
        } else if(rbAlbañil.isChecked()) {
            puesto = 2;
        } else if(rbIngObra.isChecked()) {
            puesto = 3;
        }

        if(txtNumRecibo.getText().toString().isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese su número de recibo", Toast.LENGTH_SHORT).show();
        } else if(txtNombre.getText().toString().isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese su nombre", Toast.LENGTH_SHORT).show();
        } else if(txtHorasNormal.getText().toString().isEmpty() || txtHorasExtra.getText().toString().isEmpty()) {
            if(txtHorasNormal.getText().toString().isEmpty()) {
                Toast.makeText(this, "Por favor, ingrese las horas trabajadas", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Por favor, ingrese las horas extras trabajadas", Toast.LENGTH_SHORT).show();
            }
        }

        if (!txtHorasNormal.getText().toString().isEmpty() && !txtHorasExtra.getText().toString().isEmpty() && puesto != 0) {
            horasNormales = Float.parseFloat(txtHorasNormal.getText().toString());
            horasExtra = Float.parseFloat(txtHorasExtra.getText().toString());

            subtotal = reciboN.calcularSubtotal(puesto, horasNormales, horasExtra);
            impuesto = reciboN.calcularImpuesto(puesto, horasNormales, horasExtra);
            total = reciboN.calcularTotal(puesto, horasNormales, horasExtra);
            txtSubtotal.setText("" + subtotal);
            txtImpuesto.setText("" + impuesto);
            txtTotalPagar.setText("" + total);
        } else if(puesto == 0){
            Toast.makeText(this, "Por favor, seleccione su puesto", Toast.LENGTH_SHORT).show();
        }
    }

    private void limpiar() {
        txtNumRecibo.setText("");
        txtNombre.setText("");
        txtHorasNormal.setText("");
        txtHorasExtra.setText("");
        txtSubtotal.setText("");
        txtImpuesto.setText("");
        txtTotalPagar.setText("");
    }

    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Recibo de nómina");
        confirmar.setMessage("Regresar al MainActivity");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // No se realiza nada
            }
        });
        confirmar.show();
    }

    private void deshabilitarTextos() {
        txtSubtotal.setFocusable(false);
        txtImpuesto.setFocusable(false);
        txtTotalPagar.setFocusable(false);
    }
}
